﻿

namespace LINQ.Common.DTOModels.UsersDTO
{
    public class UpdatedUserDTO
    {
        public int? NewTeamId { get; set; }
        public string NewName { get; set; }
    }
}
