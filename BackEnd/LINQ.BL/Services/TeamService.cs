﻿using AutoMapper;
using LINQ.Common.DTOModels;
using LINQ.Common.DTOModels.TeamsDTO;
using LINQ.DataAccess;
using LINQ.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LINQ.BL.Services
{
    public class TeamService : BaseService
    {
        public TeamService(IMapper mapper, LINQDbContext context) : base(context, mapper)
        {
        }
        public async Task<IEnumerable<TeamDTO>> Read()
        {
            var Teams = await _context.Teams.ToListAsync();
            return _mapper.Map<List<TeamDTO>>(Teams);
        }

        public async Task<TeamDTO> ReadById(int id)
        {
            if (!await IsExistElementById(id))
                throw new System.InvalidOperationException("Can`t find element with this id");
            return _mapper.Map<TeamDTO>(await _context.Teams.FirstAsync(t => t.Id == id));
        }

        public async System.Threading.Tasks.Task Create(TeamDTO TeamDTO)
        {
            var Team = _mapper.Map<Team>(TeamDTO);
            if (await IsExistElementById(TeamDTO.Id) || !ValidTeam(TeamDTO))
                throw new System.InvalidOperationException("tean with this id is already exist or invid data");
            _context.Teams.Add(Team);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Update(UpdatedTeamDTO newTeam, int id)
        {
            if (!await IsExistElementById(id))
                throw new System.InvalidOperationException("user with this id don`t exist");
            if(newTeam.NewName.Length>=20)
                throw new System.InvalidOperationException("name can not be larger than 20 symbols");

            var update =await _context.Teams.FirstAsync(t => t.Id == id);
            if (newTeam.NewName != "")
                update.Name = newTeam.NewName;

            _context.Teams.Update(update);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            if (!await IsExistElementById(id))
                throw new System.InvalidOperationException("Can`t find toject with this id");
            var deleted = await _context.Teams.FirstAsync(t => t.Id == id);
            _context.Teams.Remove(deleted);
            await _context.SaveChangesAsync();
        }

        private async Task<bool> IsExistElementById(int id) =>await _context.Teams.AnyAsync(t => t.Id == id);
        private bool ValidTeam(TeamDTO team) => team.CreatedAt != default;
    }
}