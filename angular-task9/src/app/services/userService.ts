import { Injectable } from "@angular/core";
import { User } from "../Models/User";
import { HttpService } from "./httpService";

@Injectable()
export class UserService{
    constructor(private httpService:HttpService){
        this.httpService.getAsync<User[]>('/users')
        .subscribe((resp)=>{
            this.users=resp;
        }, error=>{console.log(error);});
     }

    private users:User[] =[];

    public getUserByName(name:string):User{
        let rezUser:User ={firstName:'-1', id:-1};
        let rez = this.users.find(user=>user.firstName===name);
        if(rez!==undefined){
            rezUser = rez!;
        }
        return rezUser;       
    }

    public getUserById(id:number):User{
        let rezUser:User ={firstName:'-1', id:-1};
        let rez = this.users.find(user=>user.id===id);
        if(rez!==undefined){
            rezUser = rez!;
        }
        return rezUser;       
    }
}