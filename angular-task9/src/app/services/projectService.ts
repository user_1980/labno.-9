import { Injectable } from "@angular/core";
import { Project } from "../Models/Project";
import { HttpService } from "./httpService";

@Injectable()
export class ProjectService{
    constructor(private httpService:HttpService){
        this.httpService.getAsync<Project[]>('/projects')
        .subscribe((resp)=>{
            this.projects = resp;
        }, error => console.log(error));
     }

    private projects:Project[] = [];

    public getProjectByName(name:string):Project{
        let rezProject:Project ={name:'-1', id:-1};
        let rez = this.projects.find(project=>project.name===name);
        if(rez!==undefined){
            rezProject = rez!;
        }
        return rezProject;
    }
}