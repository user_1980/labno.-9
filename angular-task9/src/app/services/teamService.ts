import { Injectable } from "@angular/core";
import { Team } from "../Models/Team";
import { HttpService } from "./httpService";

@Injectable()
export class TeamService{
    getTeamById(teamId: number): Team {
        let rezTeam:Team ={name:'-1', id:-1};
        let rez = this.teams.find(team=>team.id===teamId);
        if(rez!==undefined){
            rezTeam = rez!;
        }
        return rezTeam;
    }


    constructor(private httpService:HttpService){
        this.httpService.getAsync<Team[]>('/teams')
        .subscribe((resp)=>{
            this.teams = resp;
        }, error => console.log(error));
     }

    private teams:Team[] = [];

    public getTeamByName(name:string):Team{
        let rezTeam:Team ={name:'-1', id:-1};
        let rez = this.teams.find(team=>team.name===name);
        if(rez!==undefined){
            rezTeam = rez!;
        }
        return rezTeam;
    }
}