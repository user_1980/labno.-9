import { Injectable } from "@angular/core";
import { Task } from "../Models/Task";
import { HttpService } from "./httpService";

@Injectable()
export class TaskService{
    constructor(private httpService:HttpService){
        this.httpService.getAsync<Task[]>('/tasks')
        .subscribe((resp)=>{
            this.tasks = resp;
        }, error => console.log(error));
     }

    private tasks:Task[] = [];

    public getTaskByName(name:string):Task{
        let rezTask:Task ={name:'-1', id:-1};
        let rez = this.tasks.find(task=>task.name===name);
        if(rez!==undefined){
            rezTask = rez!;
        }
        return rezTask;
    }
}