import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HttpService {

    baseUrl: string = environment.baseUrl;
    constructor(private httpClient:HttpClient){}

    public getAsync<T>(url:string):Observable<T>{
        return this.httpClient.get<T>(this.baseUrl+url);
    }

    public postAsync(url:string, httpData:any):Observable<Object>{
        console.log('sended:'+JSON.stringify(httpData));
        return this.httpClient.post(this.baseUrl+url, httpData);
    }

    public putAsync(url:string, httpData:any):Observable<Object>{
        console.log('sended:'+JSON.stringify(httpData));
        return this.httpClient.put(this.baseUrl+url, httpData);
    }
}
