import { Route } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  routerLink:string="";
  constructor() { }

  ngOnInit(): void {
  }
  public goToProjects(){
    this.routerLink="/projects";
  }
  public goToTasks(){
    this.routerLink="/tasks";
  }
  public goToUsers(){
    this.routerLink="/users";
  }
  public goToTeams(){
    this.routerLink="/teams";
  }
}
