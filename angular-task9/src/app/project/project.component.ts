import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { getElementById } from '../helpers/domHelper';
import { Project } from '../Models/Project';
import { HttpService } from '../services/httpService';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, OnDestroy {

  constructor(private httpService:HttpService) { }
  ngOnDestroy(): void {
    this.unsubscribe$.complete();
  }

  private unsubscribe$=new Subject<void>();

  public hideEditBlock:boolean=true;
  
  @Output() isSavedData = new EventEmitter<boolean>();

  @Input() project:Project = {name:'-1'};
  ngOnInit(): void {
  }

  public editProject(){
    this.hideEditBlock=!this.hideEditBlock;
    this.isSavedData.emit(this.hideEditBlock);
  }

  public updateProject(){
    let newDescription:string = (getElementById('update-description') as HTMLInputElement).value === undefined ? this.project.description! : (getElementById('update-description') as HTMLInputElement).value;
    let newDeadline:Date = (getElementById('update-deadline') as HTMLInputElement).valueAsDate===null ? this.project.deadline! : (getElementById('update-deadline') as HTMLInputElement).valueAsDate!;

    if(newDescription === this.project.description && newDeadline === this.project.deadline){
      this.hideEditBlock = true;
      this.isSavedData.emit(true);
      return;
    }

    this.httpService.putAsync('/projects/'+this.project.id!.toString(),{newDeadline:newDeadline, newDescription:newDescription})
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(()=>{
      this.project.description=newDescription;
      this.project.deadline=newDeadline;
      this.isSavedData.emit(true);
    }, error=>{console.log(error);
      alert('something went wrong');
    });
    this.hideEditBlock = true;
  }
}
