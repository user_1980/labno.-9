import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectsComponent } from './projects/projects.component';
import { TasksComponent } from './tasks/tasks.component';
import { UsersComponent } from './users/users.component';
import { TeamsComponent } from './teams/teams.component';
import { MainPageComponent } from './main-page/main-page.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './services/httpService';
import { UkraineDatePipe } from './pipes/ukraineDatePipe';
import { UserService } from './services/userService';
import { TeamService } from './services/teamService';
import { TaskService } from './services/taskService';
import { ProjectService } from './services/projectService';
import { ProjectComponent } from './project/project.component';
import { LeavePageGuard } from './guards/leavePageGuard';
import { TaskComponent } from './task/task.component';
import { UserComponent } from './user/user.component';
import { TeamComponent } from './team/team.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    TasksComponent,
    UsersComponent,
    TeamsComponent,
    MainPageComponent, 
    UkraineDatePipe,
    ProjectComponent,
    TaskComponent,
    UserComponent,
    TeamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [HttpService, UserService, TeamService, TaskService, ProjectService, LeavePageGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
