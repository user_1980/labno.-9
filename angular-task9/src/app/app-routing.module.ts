import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeavePageGuard } from './guards/leavePageGuard';
import { MainPageComponent } from './main-page/main-page.component';
import { ProjectsComponent } from './projects/projects.component';
import { TasksComponent } from './tasks/tasks.component';
import { TeamsComponent } from './teams/teams.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path: 'users', component: UsersComponent, pathMatch:'full', canDeactivate:[LeavePageGuard]},
  {path: 'tasks', component: TasksComponent,canDeactivate:[LeavePageGuard]},
  {path: 'teams', component: TeamsComponent, canDeactivate:[LeavePageGuard]},
  {path: 'projects', component: ProjectsComponent, canDeactivate:[LeavePageGuard]},
  {path:'',component:MainPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
