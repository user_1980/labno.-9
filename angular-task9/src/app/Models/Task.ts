import { State } from "./TaskState";

export interface Task{
    id?:number;
    projectId?:number;
    performerId?:number;
    name:string;
    createdAt?:Date;
    description?:string;
    finishedAt?:Date;
    state?:State;
    performerName?:string;
}