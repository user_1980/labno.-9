export interface User{
    id?:number;
    teamId?:number;
    firstName:string;
    lastName?:string;
    registeredAt?:Date;
    birthDay?:Date;
    email?:string;
}