
export interface Project{
    id?:number;
    name:string;
    description?:string;
    authorId?:number;
    teamId?:number;
    createdAt?:Date;
    deadline?:Date;
}