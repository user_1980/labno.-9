export enum State{
    Finished=1,
    AtProgress,
    NotStarted,
    Deleted
}