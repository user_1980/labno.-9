import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { getElementById } from '../helpers/domHelper';
import { Team } from '../Models/Team';
import { HttpService } from '../services/httpService';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit, OnDestroy {

  constructor(private httpService:HttpService) { }
  ngOnDestroy(): void {
    this.unsubscribe$.complete();
  }

  private unsubscribe$=new Subject<void>();

  public hideEditBlock:boolean=true;
  
  @Output() isSavedData = new EventEmitter<boolean>();
  @Input() team:Team = {name:'-1'};
  ngOnInit(): void {
  }

  public editTeam(){
    this.hideEditBlock=!this.hideEditBlock;
    this.isSavedData.emit(this.hideEditBlock);
  }

  public updateTeam(){
    let newName:string = (getElementById('update-name') as HTMLInputElement).value === undefined ? this.team.name! : (getElementById('update-name') as HTMLInputElement).value;

    if(newName === this.team.name){
      this.hideEditBlock = true;
      this.isSavedData.emit(true);
      return;
    }

    this.httpService.putAsync('/teams/'+this.team.id!.toString(),{newName:newName})
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(()=>{
      this.team.name=newName;
      this.isSavedData.emit(true);
    }, error=>{console.log(error);
      alert('something went wrong');
    });
    this.hideEditBlock = true;
  }
}
