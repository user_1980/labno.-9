import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'ukraineDate'})
export class UkraineDatePipe implements PipeTransform {
  transform(value: Date): string {
    let date:Date = new Date(value);
    let newDate:string =date.getUTCDate().toString()+' '+this.getMonth(date) +' '+date.getFullYear().toString();
    return newDate;
  }

  private getMonth(value: Date):string{
    switch (value.getMonth()){
        case 0:return 'січня';
        case 1:return 'лютого';
        case 2:return 'березеня';
        case 3:return 'квітня';
        case 4:return 'травня';
        case 5:return 'червня';
        case 6:return 'липня';
        case 7:return 'серпня';
        case 8:return 'вересня';
        case 9:return 'жовтня';
        case 10:return 'листопада';
        case 11:return 'грудня';      
        default:return '';
    }
  }
}