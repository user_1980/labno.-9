export function getElementById(id:string):HTMLElement{
    let element = document.getElementById(id);
    return element!;
}

export function getElementsByClassName(className:string):HTMLElement[]{
    return Array.from(document.getElementsByClassName(className)) as HTMLElement[];
}