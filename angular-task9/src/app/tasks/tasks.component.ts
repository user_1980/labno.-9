import { Component, Inject, Injectable, OnInit, OnDestroy } from '@angular/core';
import { Task } from '../Models/Task';
import { HttpService } from '../services/httpService';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import { getElementById } from '../helpers/domHelper';
import { UserService } from '../services/userService';
import { ProjectService } from '../services/projectService';
import { ComponentCanDeactivate } from '../guards/leavePageGuard';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, OnDestroy,ComponentCanDeactivate {

  constructor(private http:HttpService,
    private userService:UserService,
    private projectService:ProjectService) { }
  
  public tasks:Task[] = [];
  public errorMessage: string="";
  public hideCreateBlock:boolean=true;
  public errorUserMessage: string="-1";
  public errorProjectMessage: string="-1";
  public errorFinishedAtMessage:string="-1";
  public errorStateMessage:string="-1";
  public isSavedUpdatedData:boolean[]=[];
  private unsubscribe$=new Subject<void>();
  

  ngOnInit(): void {
    this.http.getAsync<Task[]>('/tasks')
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data =>{
      this.tasks = data;
    }, error=>{console.log(error.message);
      this.errorMessage="Could not load data";
    });
  }

  addData(newData: boolean) {
    if(newData){
      let index:number = this.isSavedUpdatedData.indexOf(false);
     this.isSavedUpdatedData.splice(index,1);
     return;
    }
    this.isSavedUpdatedData.push(newData);
  }
  public canDeactivate():Observable<boolean> | boolean{
     if(this.isSavedUpdatedData.filter(data=>!data).length>0){
      return confirm('You have unsaved data, you really want to leave page?');
     }
     return true;
    }

  sendCreatedTask(){
    let newTask:Task={
      name:(getElementById('name') as HTMLInputElement).value,
      description:(getElementById('description') as HTMLInputElement).value,
      createdAt:new Date(Date.now()),
      projectId:this.projectService.getProjectByName((getElementById('project-id') as HTMLInputElement).value)?.id,
      performerId:this.userService.getUserByName((getElementById('perfomer-id') as HTMLInputElement).value)?.id,    
      finishedAt:(getElementById('finished-at') as HTMLInputElement).valueAsDate!,
      state:(getElementById('state') as HTMLInputElement).valueAsNumber
  };
  if(!this.validateTask(newTask)){
    return;
  }
    this.http.postAsync('/tasks', newTask)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(()=>{
      this.tasks.push(newTask);
      this.hideCreateBlock = true;
    }, error=>{
      console.log(error);
      alert('Something went wrong ');
    });
  }

  private validateTask(task:Task):boolean{
    this.deleteErrorMessages();
    if(task.finishedAt === null){
      this.errorFinishedAtMessage='When finished is required';
      return false;
    }
    if(task.projectId===-1){
      this.errorProjectMessage = 'Did not find project with this name';
      return false;
    }
    if(task.performerId===-1){
      this.errorUserMessage = 'Did not find user with this name';
      return false;
    }
    if(task.state! <1 || task.state!>4){
      this.errorStateMessage = 'State can be less than 1 and bigger than 4';
      return false;
    }
    this.deleteErrorMessages();
    return true;
  }
  public createTask():void{
    this.hideCreateBlock=!this.hideCreateBlock;
  }

  private deleteErrorMessages():void{
    this.errorFinishedAtMessage=this.errorUserMessage=this.errorProjectMessage = this.errorStateMessage= '-1';
  }
  ngOnDestroy():void{
    this.unsubscribe$.complete;
    this.unsubscribe$.next;
  }
}
