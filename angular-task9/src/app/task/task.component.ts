import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ComponentCanDeactivate } from '../guards/leavePageGuard';
import { getElementById, getElementsByClassName } from '../helpers/domHelper';
import { Task } from '../Models/Task';
import { State } from '../Models/TaskState';
import { User } from '../Models/User';
import { HttpService } from '../services/httpService';
import { UserService } from '../services/userService';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
})
export class TaskComponent
  implements OnInit, OnDestroy
{
  constructor(
    private httpService: HttpService,
    private userService: UserService
  ) {}
  ngOnDestroy(): void {
    this.unsubscribe$.complete();
  }
  private unsubscribe$ = new Subject<void>();

  public hideEditBlock: boolean = true;
  public style:string='';

  @Input() task: Task = { name: '-1' };

  public user: string = '';
  ngOnInit(): void {
    this.user = this.userService.getUserById(this.task.performerId!).firstName;
     this.init();
  }
  @Output() isSavedData = new EventEmitter<boolean>();
  
  private init(){
      switch (this.task.state){
        case State.Deleted:this.style = "background-color: red";break;
        case State.AtProgress:this.style = "background-color :orange";break;
        case State.Finished:this.style = "background-color: green";break;
        case State.NotStarted:this.style = "background-color: black";break;
      }
  }

  public editTask() {
    this.hideEditBlock = !this.hideEditBlock;
    this.isSavedData.emit(this.hideEditBlock);
  }

  public updateTask() {
    let newDescription: string =
      (getElementById('update-description') as HTMLInputElement).value === ''
        ? this.task.description!
        : (getElementById('update-description') as HTMLInputElement).value;
    let newState: string =
      (getElementById('update-state') as HTMLInputElement).value === ''
        ? String(this.task.state!)
        : (getElementById('update-state') as HTMLInputElement).value!;
    let newPerformer: string = (
      getElementById('update-perfomer') as HTMLInputElement
    ).value;

    let user: User = this.userService.getUserByName(newPerformer);

    if (
      newDescription === this.task.description! &&
      newState === String(this.task.state!) &&
      user.id === -1
    ) {
      this.hideEditBlock = true;
      this.isSavedData.emit(true);
      return;
    }

    this.httpService
      .putAsync('/tasks/' + this.task.id!.toString(), {
        newState: newState,
        newDescription: newDescription,
        newPerformerId: user.id,
      })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        () => {
          this.task.description = newDescription;
          this.task.state = Number(newState);
          this.init();
          if(user.id !==-1){
            this.user = this.userService.getUserByName(newPerformer).firstName;
          }
          this.isSavedData.emit(true);
        },
        (error) => {
          console.log(error);
          alert('something went wrong');
        }
      );
    this.hideEditBlock = true;
  }
}
