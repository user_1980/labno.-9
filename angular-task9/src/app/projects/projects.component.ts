import { Component, Inject, Injectable, OnInit, OnDestroy } from '@angular/core';
import { Project } from '../Models/Project';
import { HttpService } from '../services/httpService';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import { getElementById } from '../helpers/domHelper';
import { UserService } from '../services/userService';
import { TeamService } from '../services/teamService';
import { ComponentCanDeactivate } from '../guards/leavePageGuard';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit, OnDestroy, ComponentCanDeactivate  {

  constructor(private http:HttpService,
    private userService:UserService,
    private teamService:TeamService) { }
  

  public isSavedUpdatedData:boolean[] = [];
  public projects:Project[] = [];
  public errorMessage: string="";
  public hideCreateBlock:boolean=true;
  public errorTeamMessage: string="-1";
  public errorUserMessage: string="-1";
  public errorDeadlineMessage:string="-1";

  private unsubscribe$=new Subject<void>();
  

  ngOnInit(): void {
    this.http.getAsync<Project[]>('/projects')
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data =>{
      this.projects = data;
    }, error=>{console.log(error.message);
      this.errorMessage="Could not load data";
    });
  }

  sendCreatedProject(){
    let newProject:Project={
      name:(getElementById('name') as HTMLInputElement).value,
      description:(getElementById('description') as HTMLInputElement).value,
      createdAt:new Date(Date.now()),
      teamId:this.teamService.getTeamByName((getElementById('team') as HTMLInputElement).value)?.id,
      authorId:this.userService.getUserByName((getElementById('author') as HTMLInputElement).value)?.id,    
      deadline:(getElementById('deadline') as HTMLInputElement).valueAsDate!
  };
  if(!this.validateProject(newProject)){
    return;
  }
    this.http.postAsync('/projects', newProject)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(()=>{
      this.projects.push(newProject);
      this.hideCreateBlock = true;
    }, error=>{
      console.log(error);
      alert('Something went wrong ');
    });
  }

  addData(newData: boolean) {
    if(newData){
      let index:number = this.isSavedUpdatedData.indexOf(false);
     this.isSavedUpdatedData.splice(index,1);
     return;
    }
    this.isSavedUpdatedData.push(newData);
  }
  public canDeactivate():Observable<boolean> | boolean{
     if(this.isSavedUpdatedData.filter(data=>!data).length>0){
      return confirm('You have unsaved data, you really want to leave page?');
     }
     return true;
    }

  private validateProject(project:Project):boolean{
    this.deleteErrorMessages();
    if(project.deadline === null){
      this.errorDeadlineMessage='Deadline is required';
      return false;
    }
    if(project.authorId===-1){
      this.errorUserMessage = 'Did not find user with this name';
      return false;
    }
    if(project.teamId===-1){
      this.errorTeamMessage = 'Did not find team with this name';
      return false;
    }
   this.deleteErrorMessages();
    return true;
  }

  private deleteErrorMessages(){
    this.errorDeadlineMessage=this.errorTeamMessage=this.errorUserMessage = '-1';
  }

  public createProject():void{
    this.hideCreateBlock=!this.hideCreateBlock;
  }

  ngOnDestroy():void{
    this.unsubscribe$.complete;
    this.unsubscribe$.next;
  }
}
