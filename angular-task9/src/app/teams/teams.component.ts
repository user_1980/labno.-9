import { Component, Inject, Injectable, OnInit, OnDestroy } from '@angular/core';
import { Team } from '../Models/Team';
import { HttpService } from '../services/httpService';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import { getElementById } from '../helpers/domHelper';
import { ComponentCanDeactivate } from '../guards/leavePageGuard';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit, OnDestroy, ComponentCanDeactivate {

  constructor(private http:HttpService,) { }
  
  public teams:Team[] = [];
  public errorMessage: string="";
  public hideCreateBlock:boolean=true;
  public errorNameMessage:string="-1";
  public isSavedUpdatedData:boolean[] = [];

  private unsubscribe$=new Subject<void>();
  

  ngOnInit(): void {
    this.http.getAsync<Team[]>('/teams')
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data =>{
      this.teams = data;
    }, error=>{console.log(error.message);
      this.errorMessage="Could not load data";
    });
  }

  sendCreatedTeam(){
    let newTeam:Team={
      name:(getElementById('team-name') as HTMLInputElement).value,
      createdAt:new Date(Date.now())
  };

  if(!this.validateTeam(newTeam)){
    return;
  }
    this.http.postAsync('/teams', newTeam)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(()=>{
      this.teams.push(newTeam);
      this.hideCreateBlock = true;
    }, error=>{
      console.log(error);
      alert('Something went wrong ');
    });
  }

  addData(newData: boolean) {
    if(newData){
      let index:number = this.isSavedUpdatedData.indexOf(false);
     this.isSavedUpdatedData.splice(index,1);
     return;
    }
    this.isSavedUpdatedData.push(newData);
  }
  public canDeactivate():Observable<boolean> | boolean{
     if(this.isSavedUpdatedData.filter(data=>!data).length>0){
      return confirm('You have unsaved data, you really want to leave page?');
     }
     return true;
    }


  private validateTeam(team:Team):boolean{
    this.deleteErrorMessages();
    if(team.name===''){
      this.errorNameMessage = 'Team name is required';
      return false;
    }
   this.deleteErrorMessages();
    return true;
  }

  private deleteErrorMessages(){
    this.errorNameMessage = '-1';
  }

  public createTeam():void{
    this.hideCreateBlock=!this.hideCreateBlock;
  }

  ngOnDestroy():void{
    this.unsubscribe$.complete;
    this.unsubscribe$.next;
  }
}
