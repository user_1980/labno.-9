import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { getElementById } from '../helpers/domHelper';
import { Team } from '../Models/Team';
import { User } from '../Models/User';
import { HttpService } from '../services/httpService';
import { TeamService } from '../services/teamService';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {

  constructor(private httpService:HttpService,
    private teamService:TeamService) { }
  ngOnDestroy(): void {
    this.unsubscribe$.complete();
  }

  private unsubscribe$=new Subject<void>();
  public team:string='';
  public hideEditBlock:boolean=true;
  
  @Output() isSavedData = new EventEmitter<boolean>();
  @Input() user:User = {firstName:'-1'};
  ngOnInit(): void {
    this.team = this.teamService.getTeamById(this.user.teamId!).name;
  }

  public editUser(){
    this.hideEditBlock=!this.hideEditBlock;
    this.isSavedData.emit(this.hideEditBlock);
  }

  public updateUser(){
    let newName:string = (getElementById('update-name') as HTMLInputElement).value === '' ? this.user.firstName! : (getElementById('update-name') as HTMLInputElement).value;
    let newTeam: string = (
      getElementById('update-team') as HTMLInputElement
    ).value==='' ? this.team :  (getElementById('update-team') as HTMLInputElement
    ).value;

    let team: Team = this.teamService.getTeamByName(newTeam);
    if(newName === this.user.firstName && team.id===-1){
      this.hideEditBlock = true;
      this.isSavedData.emit(true);
      return;
    }

    this.httpService.putAsync('/users/'+this.user.id!.toString(),{NewTeamId:team.id, newName:newName})
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(()=>{
      this.user.firstName=newName;
      this.isSavedData.emit(true);
      if(team.name !== '-1'){
     this.team = this.teamService.getTeamByName(newTeam).name;
      }
    }, error=>{console.log(error);
      alert('something went wrong');
    });
    this.hideEditBlock = true;
  }
}