import { Component, Inject, Injectable, OnInit, OnDestroy } from '@angular/core';
import { User } from '../Models/User';
import { HttpService } from '../services/httpService';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import { getElementById } from '../helpers/domHelper';
import { TeamService } from '../services/teamService';
import { ComponentCanDeactivate } from '../guards/leavePageGuard';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy, ComponentCanDeactivate {

  constructor(private http:HttpService,
    private teamService:TeamService) { }
  
  public isSavedUpdatedData:boolean[] = [];
  public users:User[] = [];
  public errorMessage: string="";
  public hideCreateBlock:boolean=true;
  public errorTeamMessage: string="-1";
  public errorEmailMessage: string="-1";
  public errorFirstNameMessage:string="-1";

  private unsubscribe$=new Subject<void>();
  

  ngOnInit(): void {
    this.http.getAsync<User[]>('/users')
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(data =>{
      this.users = data;
    }, error=>{console.log(error.message);
      this.errorMessage="Could not load data";
    });
  }

  sendCreatedUser(){
    let newUser:User={
      firstName:(getElementById('first-name') as HTMLInputElement).value,
      lastName:(getElementById('last-name') as HTMLInputElement).value,
      registeredAt:new Date(Date.now()),
      teamId:this.teamService.getTeamByName((getElementById('team') as HTMLInputElement).value)?.id,
      email:(getElementById('email') as HTMLInputElement).value,    
      birthDay:(getElementById('birthDay') as HTMLInputElement).valueAsDate!//===null ? null : (getElementById('deadline') as HTMLInputElement)!.valueAsDate
  };
  if(!this.validateUser(newUser)){
    return;
  }
    this.http.postAsync('/users', newUser)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(()=>{
      this.users.push(newUser);
      this.hideCreateBlock = true;
    }, error=>{
      console.log(error);
      alert('Something went wrong ');
    });
  }

  addData(newData: boolean) {
    if(newData){
      let index:number = this.isSavedUpdatedData.indexOf(false);
     this.isSavedUpdatedData.splice(index,1);
     return;
    }
    this.isSavedUpdatedData.push(newData);
  }
  public canDeactivate():Observable<boolean> | boolean{
     if(this.isSavedUpdatedData.filter(data=>!data).length>0){
      return confirm('You have unsaved data, you really want to leave page?');
     }
     return true;
    }

  private validateUser(user:User):boolean{
    this.deleteErrorMessages();
    if(user.email === ''){
      this.errorEmailMessage='email is required';
      return false;
    }

    if(user.firstName===''){
      this.errorFirstNameMessage = 'first name is required';
      return false;
    }
    if(user.teamId===-1){
      this.errorTeamMessage = 'Did not find team with this name';
      return false;
    }
   this.deleteErrorMessages();
    return true;
  }

  private deleteErrorMessages(){
    this.errorFirstNameMessage=this.errorTeamMessage=this.errorEmailMessage = '-1';
  }

  public createUser():void{
    this.hideCreateBlock=!this.hideCreateBlock;
  }

  ngOnDestroy():void{
    this.unsubscribe$.complete;
    this.unsubscribe$.next;
  }
}
